<?php
namespace App\Controller; //obligatoriu declarat inainte - standard

use Symfony\Component\HttpFoundation\Response; //response
use Symfony\Component\Routing\Annotation\Route; //ruta
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController; //imi da acces la metoda de render

class LuckyController extends AbstractController
{
    /**
     * @Route("/marius") 
     */
    public function number()
    {
        $number = random_int(0, 100);

        // return new Response(
        //     '<html><body>Lucky number: ' . $number . '</body></html>'
        // );

        return $this->render('lucky/number.html.twig', [
            'number' => $number,
        ]);
    }
    /**
     * @Route("/sum") 
     */
    public function sum()
    {
        $number1 = random_int(0, 100);
        $number2 = random_int(0, 100);
        return new Response(
            '<html><body>Sum of : ' . $number1 . ' + ' . $number2 . ' = ' . ($number1 + $number2) . '</body></html>'
        );
    }
    /**
     * @Route("/factorial") 
     */
    public function factorial()
    {
        $factorial = 1;
        $number = random_int(1, 10);
        for ($i = 1; $i <= $number; $i++) {
            $factorial = $i * $factorial;
        }
        return $this->render('blessed/factorial.html.twig', [
            'number' => $number,
            'factorial' => $factorial
        ]);
    }
}
