<?php

namespace App\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ShowDateController extends AbstractController
{
    /**
    * @Route("/datasiora")
    */
    public function datasiora()
    {
      $data = date('d/m/Y');
      $today = date("H:i:s");
      return $this->render('data/datasiora.html.twig', ['today' => $today, 'date'=>$data]);
    }
}
